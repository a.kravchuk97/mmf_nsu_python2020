def is_correct_brackets_seq(seq):
    stack = []
    for elem in seq:
        if elem == '(':
            stack.append(elem)
        elif elem == ')':
            if len(stack) == 0:
                return False
            stack.pop(-1)
        else:
            raise ValueError('Wrong symbol!')
    return len(stack) == 0


def number_of_unique_characters(line):
    res = {}
    for elem in line:
        if elem not in res:
            res[elem] = 0
        res[elem] += 1
    return res


def fib(n):
    fib_curr, fib_next = 0, 1
    if n <= 1:
        return n
    for i in range(n):
        fib_curr, fib_next = fib_next, fib_curr + fib_next
    return fib_curr


def merge(first, second):
    res = []
    first_pointer = 0
    second_pointer = 0
    while first_pointer < len(first) and second_pointer < len(second):
        if first[first_pointer] <= second[second_pointer]:
            res.append(first[first_pointer])
            first_pointer += 1
        else:
            res.append(second[second_pointer])
            second_pointer += 1
    res += first[first_pointer:]
    res += second[second_pointer:]
    return res


def get_card(fullname):
    surname, name, patronymic = fullname.split()
    name = f'Name: {name}'
    surname = f'Surname: {surname}'
    patronymic = f'Patronymic: {patronymic}'
    return '\n'.join(('-' * 30, f'{name:*^30}', f'{surname:*^30}', f'{patronymic:*^30}', '-' * 30))


def caesar_encrypt(message, key):
    a_order = ord('a')

    def encrypt_symb(symb):
        return a_order + (ord(symb) + key - a_order) % 26

    return ''.join(map(chr, map(encrypt_symb, message)))


