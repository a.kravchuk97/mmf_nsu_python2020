#### Слайды лекции
 * [тык](https://docs.google.com/presentation/d/1owefYWihv7FWoUCFaZN2d9FIWkB_2oUOMNkTUMy8SvQ/edit?usp=sharing)
 
 #### Полезные материалы:
 * [скачать git](https://git-scm.com)
 * [визуализатор](https://git-school.github.io/visualizing-git/)
 * [документация git'а](https://git-scm.com/docs)
 * [обучение гиту в игровой форме, очень рекомендую](https://learngitbranching.js.org/?locale=ru_RU)
 