# Репозиторий курса "Программирование на языке Python".

### Полезные ссылки

* [telegram-чатик курса](https://t.me/mmf_python_2021)
* [табличка с MR'ами](https://docs.google.com/spreadsheets/d/1uqZfs71Qyi1AennoVWDlogN1kiTDmqe8uX4OhWoVf8M/edit?usp=sharing)

### Преподаватели:
Кравчук Артем Витальевич, @akravchuk97 (tg), a.kravchuk@g.nsu.ru

Куценогий Степан Петрович, @fortotal (tg), s.kutsenogii1@g.nsu.ru

### Карта курса

* [week-01](week-01): организационные вопросы, введение в git
* [week-02](week-02): введение в python, типы и структуры данных, функции
* [week-03](week-03): декораторы
* [week-04](week-04): еще немного про структуры данных, collections
* [week-05](week-05): кодстайл, os, argparse 
* [week-06](week-06): введение в ООП
* [week-07](week-07): исключения, менеджеры контекста
* [week-08](week-08): итераторы, генераторы, itertools
* [week-09](week-09): аннотации типов, датаклассы
* [week-10](week-10): numpy
* [week-11](week-11): flask

### Дедлайны

* [hw-01](hw-01): 28.02, 23:59
* [hw-02](hw-02): 4.04, 23:59
* [hw-03](hw-03): 23.05, 23:59





