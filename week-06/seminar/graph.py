import typing as tp
from dataclasses import dataclass


@dataclass
class Node:
    id_: int
    value: str
    neigb_ids: tp.List[int]


class Graph:
    def __init__(self, nodes: tp.List[Node]):
        self._nodes = nodes
        self._id2idx = {node.id_: idx for idx, node in enumerate(nodes)}

    @property
    def adjacency_matrix(self):
        matrix = [[0] * len(self._nodes) for _ in range(len(self._nodes))]
        for node in self._nodes:
            for neigb_id in node.neigb_ids:
                matrix[self._id2idx[node.id_]][self._id2idx[neigb_id]] = 1
        return matrix

    def print_adjacency_matrix(self):
        print('\n'.join(str(line) for line in self.adjacency_matrix))

    def is_connected(self, id1, id2):
        visited = set()
        node1 = self._nodes[self._id2idx[id1]]
        node2 = self._nodes[self._id2idx[id2]]
        return self._is_connected(node1, node2, visited)

    def _is_connected(self, node1: Node, node2: Node, visited: set):
        visited.add(node1.id_)
        for neigb_id in node1.neigb_ids:
            if neigb_id in visited:
                continue
            visited.add(neigb_id)
            if neigb_id == node2.id_:
                return True
            return self._is_connected(self._nodes[self._id2idx[neigb_id]], node2, visited)
        return False


if __name__ == '__main__':
    nodes = [
        Node(id_=1, value='2', neigb_ids=[2, 10]),
        Node(id_=2, value='2', neigb_ids=[1]),
        Node(id_=10, value='2', neigb_ids=[1]),
        Node(id_=20, value='2', neigb_ids=[4]),
        Node(id_=4, value='2', neigb_ids=[20]),
    ]
    graph = Graph(nodes)
    print(graph.adjacency_matrix)
    graph.print_adjacency_matrix()
    print(graph.is_connected(1, 2))
    print(graph.is_connected(1, 4))
