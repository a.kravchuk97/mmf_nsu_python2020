import json
from datetime import datetime


class EventsCalendar:
    def __init__(self, events: dict):
        self._events = events
        for event_name, event_date in events.items():
            event_date, _ = event_date.split()
            year, month, day = map(int, event_date.split('/'))
            self._events[event_name] = datetime(year=year, month=month, day=day)

    def print_time_before_actual_events(self):
        event_name2n_days = self._get_time_before_actual_events()
        for event_name, n_days in event_name2n_days.items():
            print(f'{n_days} before event {event_name}')

    def _get_time_before_actual_events(self):
        curr_time = datetime.now()
        res = {}
        for event_name, event_date in self._events.items():
            n_days = (event_date - curr_time).days
            if n_days > 0:
                res[event_name] = n_days
        return res

    @classmethod
    def from_json_path(cls, path):
        print(cls)
        with open(path) as f:
            events = json.load(f)
        return cls(events)


class BeautyEventsCalendar(EventsCalendar):
    def print_time_before_actual_events(self):
        print('*' * 20)
        super().print_time_before_actual_events()
        print('*' * 20)


if __name__ == '__main__':
    calendar = BeautyEventsCalendar(
        {
            'dr mami': '2021/08/01 21:12',
            'ng': '2022/01/01 00:00',
            'davno': '2000/01/01 00:00'
        }
    )
    calendar.print_time_before_actual_events()
    calendar = EventsCalendar.from_json_path("events.json")
    calendar.print_time_before_actual_events()
