class Registry:
    def __init__(self, name):
        self._name = name
        self._cache = {}

    def register(self, name, value):
        self._cache[name] = value

    def get(self, name):
        return self._cache[name]

    def flush(self):
        self._cache = {}


if __name__ == '__main__':
    registry = Registry('registry')
    registry.register('Registry', Registry)
    registry.register('registry', registry)
    print(registry.get('registry'))
