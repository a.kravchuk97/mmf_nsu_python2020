import argparse
import os


def rmrf(dir_path):
    directories = []
    files = []
    for fname in os.listdir(dir_path):
        if os.path.isdir(os.path.join(dir_path, fname)):
            directories.append(fname)
        else:
            files.append(fname)
    for file in files:
        os.remove(os.path.join(dir_path, file))
    for directory in directories:
        rmrf(os.path.join(dir_path, directory))
    os.rmdir(dir_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir_path', type=str, required=True)
    args = parser.parse_args()
    rmrf(args.dir_path)
