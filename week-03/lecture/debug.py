from functools import wraps

DEBUG = True


def trace(f):
    if not DEBUG:
        return f

    @wraps(f)
    def wrapper(*args, **kwargs):
        res = f(*args, **kwargs)
        print(f'{f.__name__}, args: {args}, kwargs: {kwargs}, res: {res}')
        return res

    return wrapper


@trace
def my_sum(*args):
    res = 0
    for arg in args:
        res += arg
    return res


@trace
def my_max(*args):
    res = float('-inf')
    for arg in args:
        res = max(res, arg)
    return res


def max_of_sums(*lists):
    sums = []
    for list_ in lists:
        sums.append(my_sum(*list_))
    return my_max(*sums)
