import functools
import random
from time import time

CACHE = True


def profile(f):
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        t_start = time()
        res = f(*args, **kwargs)
        wrapper._time += time() - t_start
        return res

    wrapper._time = 0
    return wrapper


def cache(f):
    if not CACHE:
        return f
    _cache = {}

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        key = (args, frozenset(kwargs.items()))
        if key in _cache:
            return _cache[key]
        res = f(*args, **kwargs)
        _cache[key] = res
        return res

    return wrapper


@profile
@cache
def fib(n):
    if n <= 1:
        return n
    return fib(n - 1) + fib(n - 2)


@profile
def get_random_number():
    return random.randint(10, 20)


def get_random_fib():
    return fib(get_random_number())
