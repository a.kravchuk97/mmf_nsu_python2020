from functools import wraps
from warnings import warn


def deprecated(message):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            warn(message)
            return f(*args, **kwargs)

        return wrapper

    return decorator


@deprecated('ninada menya ispolzovat')
def f(x):
    return x


if __name__ == '__main__':
    f(1)
